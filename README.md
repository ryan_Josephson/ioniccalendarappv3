# RJ's Postal Calendar

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
ionic serve
```

### Compiles and minifies for production

```
ionic build
npx cap sync
```

### Modify Files

```
change cordova-plugin-inapppurchase\src\android\InAppBillingV3.java
line 69 from "www/manifest.json" to "public/manifest.json"

```
