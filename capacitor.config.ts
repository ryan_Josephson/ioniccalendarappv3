import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.cordova.rjpostalcalendar',
  appName: `RJ's Postal Calendar`,
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
