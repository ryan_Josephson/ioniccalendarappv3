import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'welcome',
    loadChildren: () =>
      import('./welcome/welcome.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'calendar',
    loadChildren: () =>
      import('./common/pages/calendar/calendar.module').then(
        (m) => m.CalendarPageModule
      ),
  },
  {
    path: 'event',
    loadChildren: () =>
      import('./common/pages/event/event.module').then(
        (m) => m.EventPageModule
      ),
  },
  {
    path: 'purchase',
    loadChildren: () =>
      import('./common/pages/purchase/purchase.module').then(
        (m) => m.PurchasePageModule
      ),
  },
  {
    path: 'alert',
    loadChildren: () =>
      import('./common/pages/alert/alert.module').then(
        (m) => m.AlertPageModule
      ),
  },
  {
    path: 'toggle-date',
    loadChildren: () => import('./common/pages/toggle-date/toggle-date.module').then( m => m.ToggleDatePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
