import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormatToLocalPipe } from './formatToLocal/format-to-local.pipe';
import { FormatToStylePipe } from './formatToStyle/format-to-style.pipe';
import { FormatToStyle2Pipe } from './formatToStyle2/format-to-style2.pipe';
import { FormatToStyle3Pipe } from './formatToStyle3/format-to-style3.pipe';
@NgModule({
  declarations: [FormatToLocalPipe, FormatToStylePipe, FormatToStyle2Pipe, FormatToStyle3Pipe],
  imports: [],
  exports: [FormatToLocalPipe, FormatToStylePipe, FormatToStyle2Pipe, FormatToStyle3Pipe]
})
export class PipesModule {}
