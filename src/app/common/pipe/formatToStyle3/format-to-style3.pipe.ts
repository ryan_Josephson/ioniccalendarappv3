import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatToStyle3'
})
export class FormatToStyle3Pipe implements PipeTransform {

  transform(val) {
    if (val) {
      return moment(val).format('hh:mm A');
    }
  }

}
