interface IPaydayInfo {
    data: any[string];
    currentPayday: any[string];
  }
  
  export class PaydayInfo implements IPaydayInfo {
    public data: any[string] = [];
    public currentPayday: any[string] = [];
    constructor(reqData: IPaydayInfo = null) {
      this.data = reqData && reqData.data || null;
      this.currentPayday = reqData && reqData.currentPayday || null;
    }
  
  }