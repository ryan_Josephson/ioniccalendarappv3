interface IEventInfo {
    eventSource: any[];
    currentEvent: any[];
  }
  
  export class EventInfo implements IEventInfo {
    public eventSource: any[];
    public currentEvent: any[];
    constructor(reqData: IEventInfo = null) {
      this.eventSource = reqData && reqData.eventSource || null;
      this.currentEvent = reqData && reqData.currentEvent || null;
    }
  
  }
  