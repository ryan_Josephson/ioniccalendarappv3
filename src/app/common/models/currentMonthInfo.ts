interface ICurrentMonthInfo {
    year: number;
    currentDay: Date;
    month: string;
  }
  
  export class CurrentMonthInfo implements ICurrentMonthInfo {
    public year: number;
    public currentDay: Date;
    public month: string;
    constructor(reqData: ICurrentMonthInfo = null) {
      this.year = reqData && reqData.year || null;
      this.currentDay = reqData && reqData.currentDay || new Date();
      this.month = reqData && reqData.month || null;
    }
  
  }
  