import { Injectable } from '@angular/core';
import { PaydayInfo } from '../../common/models/paydays';
import { CurrentMonthInfo } from '../../common/models/currentMonthInfo';
import { EventInfo } from '../../common/models/events';
import { setHol } from '../utils/holidays';
import * as moment from 'moment';
import { getPattern } from '../utils/colorPattern';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  currentWelcomedOnce = true;
  event: EventInfo = new EventInfo();
  payday: PaydayInfo = new PaydayInfo();
  date: CurrentMonthInfo = new CurrentMonthInfo();
  monthNames = [];
  currentDay = new Date();
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  lastYear: number;
  nextYear: number;
  currentYear = 2022;
  color: any[] = ['#4d4dff', 'green', '#733100', 'red', 'black', '#FFEC00'];
  darkMode: boolean = false;
  selectEvent: string;
  actualYear: number = new Date().getFullYear();
  holiday = [];
  currentHoliday: [{ name: string; date: Date }];
  pattern: any;
  patternWeek: number;

  constructor(public storage: StorageService) {
    this.monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    this.setEventStorage();
    this.getDaysOfMonth();
    this.getPayday();
    this.paydayThisMonth();
    this.getHoliday();
    this.loadHolidayThisMonth();
    this.getDistrictColor();
    // this.pattern = getPattern(this.date.year);
    this.pattern = getPattern();
    const found = this.pattern.find((e) => e.year === this.date.year);
    this.patternWeek = found.p;
    this.getDistrictColor();
    this.getDarkMode();
  }
  async getDistrictColor() {
    const theme: any = await this.storage.get('theme');
    if (!theme) {
      this.color = ['#4d4dff', 'green', '#733100', 'red', 'black', '#FFEC00'];
      this.selectEvent = '385';
    } else {
      this.color = theme.color;
      this.selectEvent = theme.selectEvent;
    }
  }
  async getDarkMode() {
    const theme = await this.storage.get('darkmode');
    if (!theme) {
      this.darkMode = theme;
      document.body.classList.remove('dark');
    } else {
      document.body.classList.add('dark');
      this.darkMode = document.body.className === 'dark' ? true : false;
    }
  }
  setDistrictColor(district: any[]) {
    this.color = district;
  }
  setWelcomeStatus() {
    this.storage.set('wasWelcomeOnceV2', true);
  }
  getPaydayData() {
    return this.payday;
  }
  getHoliday() {
    this.holiday = setHol(this.date);
  }
  getPayday() {
    this.payday.data = [];
    const firstPayday = moment()
      .year(2022)
      .month('January')
      .date(7)
      .dayOfYear();
    const lastDayOfYear = moment()
      .year(2022)
      .month('December')
      .date(31)
      .dayOfYear();
    let nextFirst = lastDayOfYear;
    let dayDiff = 0;
    const yearDiff = this.date.year - 2022;
    const yearAmount = this.date.year - yearDiff;
    for (let i = this.date.year; i > yearAmount; i--) {
      nextFirst += moment().year(i).month('December').date(31).dayOfYear();
    }
    for (let i = firstPayday; i <= nextFirst; i += 14) {
      dayDiff = i;
    }
    for (let i = 2022; i < this.date.year; i++) {
      dayDiff -= moment().year(i).month('December').date(31).dayOfYear();
    }
    for (let i = dayDiff; i >= 0; i -= 14) {
      let evenPayDay;
      evenPayDay = moment()
        .year(this.date.year)
        .dayOfYear(i)
        .format('MMM DD, YYYY');
      this.payday.data.push(evenPayDay);
    }
    this.payday.data.sort(function (x: any, y: any) {
      let a: any = new Date(x);
      let b: any = new Date(y);
      return a - b;
    });
    return;
  }
  paydayThisMonth() {
    this.payday.currentPayday = [];
    this.payday.data.forEach((event) => {
      const l = new Date(event);
      if (
        this.monthNames[l.getMonth()] === this.date.month &&
        l.getFullYear() === this.date.year
      ) {
        this.payday.currentPayday.push(event);
        return true;
      } else {
        return false;
      }
    });
  }
  getDaysOfMonth() {
    const currentDay = this.date.currentDay;
    let currentDate;
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.date.month = this.monthNames[currentDay.getMonth()];
    this.date.year = currentDay.getFullYear();
    if (this.date.year !== this.currentYear) {
      this.currentYear = this.date.year;
      this.getHoliday();
      this.getPayday();
      if (!this.pattern) {
        this.pattern = getPattern();
      }
      const found = this.pattern.find((e) => e.year === this.date.year);
      this.patternWeek = found.p;
    }
    this.nextYear = this.date.year + 1;
    this.lastYear = this.date.year - 1;
    if (
      currentDay.getMonth() === new Date().getMonth() &&
      currentDay.getFullYear() === this.actualYear
    ) {
      currentDate = new Date().getDate();
    } else {
      currentDate = 999;
    }

    const firstDayThisMonth = new Date(
      currentDay.getFullYear(),
      currentDay.getMonth(),
      1
    ).getDay();
    const prevNumOfDays = new Date(
      currentDay.getFullYear(),
      currentDay.getMonth(),
      0
    ).getDate();
    for (
      let i = prevNumOfDays - (firstDayThisMonth - 1);
      i <= prevNumOfDays;
      i++
    ) {
      this.daysInLastMonth.push(i);
    }

    const thisNumOfDays = new Date(
      currentDay.getFullYear(),
      currentDay.getMonth() + 1,
      0
    ).getDate();
    for (let j = 0; j < thisNumOfDays; j++) {
      this.daysInThisMonth.push(j + 1);
    }

    const lastDayThisMonth = new Date(
      currentDay.getFullYear(),
      currentDay.getMonth() + 1,
      0
    ).getDay();
    for (let k = 0; k < 6 - lastDayThisMonth; k++) {
      this.daysInNextMonth.push(k + 1);
    }
    const totalDays =
      this.daysInLastMonth.length +
      this.daysInThisMonth.length +
      this.daysInNextMonth.length;
    // console.log('lastmonth', this.daysInLastMonth.length, 'daysinthismonth',
    //this.daysInThisMonth.length, 'nextmonth', this.daysInNextMonth.length);
    if (totalDays < 35) {
      for (let l = 7 - lastDayThisMonth; l < 7 - lastDayThisMonth + 7; l++) {
        this.daysInNextMonth.push(l);
      }
    }
  }
  changeMonth(t) {
    this.date.currentDay = t;
    this.getDaysOfMonth();
    this.paydayThisMonth();
    this.loadHolidayThisMonth();
    this.loadEventThisMonth();
  }
  loadEventThisMonth() {
    this.event.currentEvent = [];
    if (this.event.eventSource === null) {
      this.event.eventSource = [];
    }
    this.event.eventSource.forEach((event) => {
      const r = moment(event.startTime).format('MMMM');
      const t = moment(event.endTime).format('MMMM');

      if (
        r === this.date.month &&
        moment(event.startTime).year() === this.date.year
      ) {
        this.event.currentEvent.push(event);
      } else if (
        t === this.date.month &&
        moment(event.endTime).year() === this.date.year
      ) {
        this.event.currentEvent.push(event);
      } else {
      }
    });
  }
  loadHolidayThisMonth() {
    this.currentHoliday = [{ name: 'blah', date: new Date('January 1,2016') }];
    let testForIndex = true;

    this.holiday.forEach((event) => {
      const t = moment(event.date).format('MMMM');
      const p = { name: event.name, date: event.date };
      // console.log(this.date.month + ' event ' + t + ' ' + this.date.year + ' ' + moment(event.date).year());
      if (
        t === this.date.month &&
        moment(event.date).year() === this.date.year
      ) {
        if (testForIndex) {
          this.currentHoliday.push(p);
          this.currentHoliday.splice(0, 1);
          testForIndex = false;
        } else {
          this.currentHoliday.push(p);
        }
      }
    });
    if (testForIndex) {
      this.currentHoliday = null;
    }
  }
  saveEvent(data) {
    this.event.eventSource = [];
    setTimeout(() => {
      this.event.eventSource = data;
      this.storage.set('event', this.event.eventSource);
      this.loadEventThisMonth();
    });
  }
  deleteEvent(item: number) {
    this.event.eventSource.splice(item, 1);
    this.storage.set('event', this.event.eventSource);
    this.loadEventThisMonth();
  }
  async setEventStorage() {
    this.event.eventSource = [];
    const event: any = await this.storage.get('event');
    if (event) {
      this.event.eventSource = event;
      this.loadEventThisMonth();
    }
  }
  editEvent(newEvent: [], testForNewEvent) {
    if (testForNewEvent === 'Add') {
      this.event.eventSource.push(newEvent);
      this.storage.set('event', this.event.eventSource);
      this.loadEventThisMonth();
    } else {
      // for (let i = 0; i < this.event.eventSource.length; i++) {
      //   if (this.event.eventSource[i] === oldEvent) {
      //     this.event.eventSource.splice(i, 1);
      //   }
      // }
      // this.event.eventSource.push(newEvent);
      this.storage.set('event', this.event.eventSource);
      this.loadEventThisMonth();
    }
  }
}
