import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage-angular';
import * as CordovaSQLiteDriver from 'localforage-cordovasqlitedriver';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private _storage: Storage | null = null;

  constructor(private storage: Storage, private router: Router) {
    this.init();
  }
  async init() {
    const storage = await this.storage.create();
    const storage2 = await this.storage.defineDriver(CordovaSQLiteDriver);
    this._storage = storage;
  }

  public set(key: string, value: any) {
    this._storage?.set(key, value);
  }
  public get(key: string) {
    return this._storage?.get(key);
  }
  public store() {
    this.storage.forEach((key, value, index) => {
      console.log(key, value, index);
    });
  }
  public async clear() {
    await this.storage.remove('wasWelcomeOnceV2');
  }
  welcome() {
    // this.setWelcomed();
    const welcome: any = this.wasWelcomed().then((value) => {
      if (value) {
        this.router.navigateByUrl('/home');
      } else {
        this.router.navigateByUrl('/welcome');
        this.setWelcomed();
      }
    });
  }
  async wasWelcomed() {
    // this.clear();
    return await this.storage.get('wasWelcomeOnceV2');
  }
  async setWelcomed() {
    await this.storage.set('wasWelcomeOnceV2', true);
    await this.storage.remove('theme');
    await this.storage.remove('wasWelcomeOnce');
  }
}
