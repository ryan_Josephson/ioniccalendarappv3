import { Storage } from '@ionic/storage-angular';
import * as moment from 'moment';
import { CurrentMonthInfo } from '../models/currentMonthInfo';
export function getColor(
  day: number,
  currentMonthInfo: CurrentMonthInfo,
  theme: any[],
  pattern
) {
  let color = theme;
  let currentWeek: Date | number = new Date(
    currentMonthInfo.year,
    moment(currentMonthInfo.month, 'MMMM').month(),
    day
  );
  let week = 0;
  const curDate = currentWeek;
  if (moment(curDate).weekYear() != moment(curDate).year()) {
    pattern -= 2;
    if (pattern % 2 === 0) {
      if (pattern < 0) {
        pattern = 4;
      }
    } else {
      if (pattern < 1) {
        pattern = 5;
      }
    }
  }
  week = moment(currentWeek).week() + pattern;
  week %= 6;
  switch (week) {
    case 0:
      color = [color[3], color[4], color[5], color[0], color[1], color[2]];
      break;
    // return color; red black yellow blue green brown
    case 1:
      color = [color[2], color[3], color[4], color[5], color[0], color[1]];
      break;
    // return color; brown red black yellow blue green
    case 2:
      color = [color[1], color[2], color[3], color[4], color[5], color[0]];
      // return color; green brown red black yellow blue
      break;
    case 3:
      color = [color[0], color[1], color[2], color[3], color[4], color[5]];
      break;
    // return color; blue green brown red black yellow
    case 4:
      color = [color[5], color[0], color[1], color[2], color[3], color[4]];
      break;
    // return color; yellow blue green brown red black
    case 5:
      color = [color[4], color[5], color[0], color[1], color[2], color[3]];
      break;
    // return color; black yellow blue green brown red
  }
  switch (moment(curDate).day()) {
    case 0: // sun
      return color[1];
    case 1: // mon
      return color[2];
    case 2: // tue
      return color[3];
    case 3: // wed
      return color[4];
    case 4: // thu
      return color[5];
    case 5: // fri
      return color[0];
    case 6: // sat
      return color[0];
  }
}
