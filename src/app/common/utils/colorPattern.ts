import * as moment from 'moment';
export function getPattern() {
    let change = 0;
    let counter = 0;
    let pattern = 5;
    let y = [{year: 2015, p: 5}]
    for (let i = 2016; i <= 2140; i++) {
      
      if (counter === 1) {
        pattern -= 1;
        change++;
      } else if (counter === 7) {
        pattern -= 1;
        change++;
      } else if (counter === 13) {
        pattern -= 1;
        change++;
      } else if (counter === 18) {
        pattern -= 1;
        change++;
      } else if (counter === 24) {
        pattern -= 1;
        change++;
        counter = -4;
      } else {
        pattern -= 2;
      }
      counter++
      if (pattern % 2 === 0) {
        if (pattern < 0) {
          pattern = 4;
        }
      } else {
        if (pattern < 1) {
          pattern = 5;
        }
      }
      y.push({year:i, p:pattern});
      
    }
    return y;
}