import * as moment from 'moment';
import { CurrentMonthInfo } from '../models/currentMonthInfo';
import { Holiday } from '../models/holidays';
let holiday = [];
function getHoliday(year:number) {
    for(let i = 0; i < 12; i++) {
        switch (i) {
            case 0: //January
                holiday.push(new Holiday('New Years Day', new Date(year, i, 1))) // January 1
                setHoliday(year, i, 3, 1, 'Martin Luther'); // third monday
            break;
            case 1: //February
                setHoliday(year, i, 3, 1,"President's Day"); // third monday
            break;
            case 4: //May
                setHoliday(year, i, 0, 1,"Memorial Day"); // last monday
            break;
            case 5: //June
                holiday.push(new Holiday("Juneteenth", new Date(year, i, 19))); // June 19th
                break;
            case 6: //July
                holiday.push(new Holiday("Independence Day", new Date(year, i, 4))); // July 4th
            break;
            case 8: //September
                setHoliday(year, i, 1, 1,"Labor Day"); // first monday
            break;
            case 9: //October
                setHoliday(year, i, 2, 1,"Indigenous Peoples' Day"); // second monday
                // holiday.push(new Holiday("Halloween", new Date(year, i, 31))); // October 31
            break;
            case 10: //November
                holiday.push(new Holiday("Veterans Day", new Date(year,i, 11))); // November 11
                setHoliday(year, i, 4, 4,"Thanksgiving Day"); // fourth thursday
            break;
            case 11: //December
                holiday.push(new Holiday("Christmas", new Date(year,i, 25))); // December 25
            break;

        }

    }
}
function setHoliday(year, month, whatWeek, dayOfWeek, holidayName) {
    let weekCounter = 0;
    let hol;
    let daysInMonth = moment().year(year).month(month).daysInMonth();
    for (let i = 1; i <= daysInMonth; i++) {
      if(moment().year(year).month(month).date(i).day() === dayOfWeek) {
        if(whatWeek !== 0) {
          weekCounter++;
          if(weekCounter === whatWeek) {
            // holiday.push(new Holiday(holidayName, new Date(year, month, i)));
            hol = new Holiday(holidayName, new Date(year, month, i));
          }
        } else {
            // holiday.push(new Holiday(holidayName, new Date(year, month, i)));
            hol = new Holiday(holidayName, new Date(year, month, i));
        }
      }
    }
    holiday.push(hol);
}
export function setHol(currentMonthInfo: CurrentMonthInfo) {
    holiday = [];
    getHoliday(currentMonthInfo.year);
    return holiday;
}