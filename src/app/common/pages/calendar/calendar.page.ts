import { Component, OnInit } from '@angular/core';
import { ModalController, IonItemSliding, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { getColor } from '../../utils/colors';
import { DataService } from '../../services/data.service';
import { EventPage } from '../event/event.page';
import { ToggleDatePage } from '../toggle-date/toggle-date.page';
import { element } from 'protractor';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.page.html',
  styleUrls: ['./calendar.page.scss'],
})
export class CalendarPage implements OnInit {
  selectedDay = new Date();
  eventSource = [];
  currentMonthEvents = [];
  eventList: any;
  selectedEvent: any;
  isSelected: any;
  buttonClicked: boolean;
  segmentButton: any;
  color: [];
  years = [2022, 2023, 2024, 2025, 2026];
  constructor(
    private storage: Storage,
    public data: DataService,
    public modalCtrl: ModalController,
    public platform: Platform
  ) {}
  swipeLeft(event) {
    this.goToNextMonth();
  }
  swipeRight(event) {
    this.goToLastMonth();
  }
  ngOnInit() {
    console.log('loaded');
    this.platform.resume.subscribe(async () => {
      this.data.getDarkMode();
    });
  }
  getStyleColor(day: number) {
    const color = getColor(
      day,
      this.data.date,
      this.data.color,
      this.data.patternWeek
    );
    if (color === 'black') {
      const e = document.getElementById(day.toString());
      if (e) {
        const parentHasClass = e.closest('.currentDate') !== null;
        if ((parentHasClass && e.className === 'day-of') || (parentHasClass && e.className === 'day-of text-shadow-black')) {
          const c = document.getElementById('currentDate');
          c.parentElement.className += ' border-shadow';
        }
        if (e.className === 'day-of') {
          e.className += ' text-shadow-black';
        }
      }
    } else {
      
      const e = document.getElementById(day.toString());
      if (e) {
        const parentHasClass = e.closest('.currentDate') !== null;
        if (e.className === 'day-of text-shadow-black') {
          e.classList.remove('text-shadow-black');
        } else if (e.className === 'day-of text-shadow-blue') {
          e.classList.remove('text-shadow-blue');
        }
        if (parentHasClass) {
          const c = document.getElementById('currentDate');
          if (c.parentElement.className === 'currentDateBorder border-shadow') {
            c.parentElement.classList.remove('border-shadow');
          }
        }
      }
    }
    return color;
  }
  formatEventSource() {
    this.eventSource.forEach((event) => {
      event.startTime = new Date(event.startTime);
      event.endTime = new Date(event.endTime);
    });
  }

  checkEvent(day: number) {
    let hasEvent = false;
    const date1 = new Date(
      this.data.date.year,
      moment(this.data.date.month, 'MMMM').month(),
      day
    );
    if (this.data.event.currentEvent === null) {
      this.data.event.currentEvent = [];
    }
    this.data.event.currentEvent.forEach((event) => {
      const date2 = moment(date1).dayOfYear();
      const startTime = moment(event.startTime).dayOfYear();
      const endTime = moment(event.endTime).dayOfYear();
      if (moment(event.startTime).year() === this.data.date.year) {
        const test = endTime - startTime;
        if (startTime <= date2 && date2 <= endTime) {
          hasEvent = true;
        }
      }
    });
    return hasEvent;
  }
  checkPayday(day) {
    let hasPayday = false;
    const datePayday = new Date(
      this.data.date.year,
      moment(this.data.date.month, 'MMMM').month(),
      day
    );
    this.data.payday.currentPayday.forEach((event) => {
      if (
        event === moment(datePayday).format('MMM DD, YYYY') &&
        moment(datePayday).weekday() === 5
      ) {
        hasPayday = true;
      }
    });

    return hasPayday;
  }
  checkHoliday(day) {
    let hasHoliday = false;
    const thisHoliday = new Date(
      this.data.date.year,
      moment(this.data.date.month, 'MMMM').month(),
      day
    );
    this.data.holiday.forEach((event) => {
      if (thisHoliday.toString() === event.date.toString()) {
        hasHoliday = true;
      }
    });

    return hasHoliday;
  }
  goToLastMonth() {
    const t = new Date(
      this.data.date.year,
      moment(this.data.date.month, 'MMMM').month() - 1,
      1
    );
    this.data.changeMonth(t);
  }

  goToNextMonth() {
    const t = new Date(
      this.data.date.year,
      moment(this.data.date.month, 'MMMM').month() + 1,
      1
    );
    this.data.changeMonth(t);
  }
  async dropDownModal() {
    const popover = await this.modalCtrl.create({
      component: ToggleDatePage,
      componentProps: {
        months: this.data.monthNames,
        year: this.data.actualYear,
        selectedYear: this.data.date.year,
      },
      animated: true,
      cssClass: 'my-custom-date-selector-css',
    });
    await popover.present();
    popover.onDidDismiss().then((data: any) => {
      if (data.data) {
        const t = new Date(
          data.data.year,
          moment(data.data.month, 'MMMM').month(),
          1
        );
        this.data.changeMonth(t);
      }
    });
  }
  async selectDate(day) {
    const clickEvent = this.data.event.currentEvent;
    const popover = await this.modalCtrl.create({
      component: EventPage,
      componentProps: {
        currentEvent: clickEvent,
        selectedDay: this.data.date.currentDay,
        aDay: day,
        popoverController: true,
      },
      // event: day,
      animated: true,
      cssClass: 'my-custom-modalCtrl-css',
    });
    await popover.present();
    popover.onDidDismiss().then((data: any) => {
      if (data.data) {
        const eventData: any = data.data.event;
        eventData.startTime = new Date(data.data.event.startTime);
        eventData.endTime = new Date(data.data.event.endTime);
        this.data.editEvent(data.data.event, data.data.newEvent);
      } else {
        this.data.setEventStorage();
      }
    });
  }
  async removeItem(item, event: IonItemSliding) {
    const openAmount = await event.getOpenAmount();
    if (openAmount >= 140) {
      for (let i = 0; i < this.eventSource.length; i++) {
        if (this.eventSource[i] === item) {
          this.eventSource.splice(i, 1);
          this.storage.set('event', this.eventSource);
        }
      }
    }
  }
}
