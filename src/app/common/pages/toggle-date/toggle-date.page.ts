import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-toggle-date',
  templateUrl: './toggle-date.page.html',
  styleUrls: ['./toggle-date.page.scss'],
})
export class ToggleDatePage implements OnInit {
  months: [];
  toggleYear;
  currentYear: number;
  selectedYear: number;
  constructor(
    public navParams: NavParams,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.months = this.navParams.get('months');
    this.currentYear = this.navParams.get('year');
    this.selectedYear = this.navParams.get('selectedYear');
    this.generateYears();
  }

  save(selectedMonth) {
    this.modalCtrl.dismiss({ month: selectedMonth, year: this.selectedYear });
  }
  newYear(event) {
    this.selectedYear = event.detail.value;
  }
  generateYears() {
    const setYears = this.currentYear + 15;
    let data = [];
    for (let i = this.currentYear; i <= setYears; i++) {
      data.push(i);
    }
    this.toggleYear = data;
  }
  cancel() {
    this.modalCtrl.dismiss();
  }
}
