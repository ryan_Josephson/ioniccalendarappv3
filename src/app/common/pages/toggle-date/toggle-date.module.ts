import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ToggleDatePageRoutingModule } from './toggle-date-routing.module';

import { ToggleDatePage } from './toggle-date.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ToggleDatePageRoutingModule
  ],
  declarations: [ToggleDatePage]
})
export class ToggleDatePageModule {}
