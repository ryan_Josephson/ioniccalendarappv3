import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ToggleDatePage } from './toggle-date.page';

const routes: Routes = [
  {
    path: '',
    component: ToggleDatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ToggleDatePageRoutingModule {}
