import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { EventPage } from '../event.page';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  event = { title: '', startTime: new Date(), endTime: new Date() };
  minDate = new Date().getFullYear();
  formButton = 'Add';
  edit = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private selectEvent: EventPage,
    private modalCtrl: ModalController
  ) {
    // const preselectedDate = this.navParams.get('selectedDay');
    if (!this.navParams.get('modalController')) {
      if (this.selectEvent.isThereAnEvent()) {
        this.formButton = 'Edit';
        this.edit = true;
      }
    }
    // console.log(preselectedDate);
    this.event = this.selectEvent.getEvent();
  }
  save(form) {
    this.modalCtrl.dismiss({ event: this.event, newEvent: form });
  }
  cancel() {
    this.modalCtrl.dismiss();
  }
  delete() {
    console.log(this.event);
  }
  onChange(events) {
    this.event.endTime = events;
    return events;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEventPage');
  }

  ngOnInit() {
    // Keyboard.show();
  }
}
