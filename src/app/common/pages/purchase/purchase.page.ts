import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';


@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.page.html',
  styleUrls: ['./purchase.page.scss'],
  providers: [InAppPurchase]
})
export class PurchasePage implements OnInit {
  products: any;
  products2: any;
  isLoading = true;
  purchased = false;
  valueDetail = '1dollar';
  constructor(private popoverCtrl: PopoverController, private iap: InAppPurchase) { }
  ngOnInit() {
  }
  ionViewDidEnter() {
    this.iap
    .getProducts(['1dollar', '2dollars', '5dollars', '10dollars'])
    .then((products) => {
      this.products = products;
      this.isLoading = false;
      return products;
    })
    .catch((err) => {
      console.log(err);
    });
    this.products2 = [{productId: '1dollar', price: '0.99'}, {productId: '2dollar', price: '1.99'},
    {productId: '5dollar', price: '4.99'}, {productId: '10dollar', price: '9.99'} ];
  }
  // buy(product) {
  //   console.log(product);
  // }
  productSelect(event) {
    this.valueDetail = event.detail.value;
  }
  cancel() {
    this.popoverCtrl.dismiss();
  }
 purchase() {
  }
  buy(product) {
    console.log(product);
    this.iap
    .buy(product)
    .then((data) => this.iap.consume(data.productType, data.receipt, data.signature))
    //   // ...then mark it as consumed:
    //   return this.iap.consume(data.productType, data.receipt, data.signature);
    // })
    .then(() => {
      console.log('product was successfully consumed!');
      this.purchased = true;
    })
    .catch((err) => {
      console.log(err);
    });
  }
}
