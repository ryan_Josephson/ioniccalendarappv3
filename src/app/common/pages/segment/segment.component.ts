import { Component, OnInit, ViewChild } from '@angular/core';
import { IonItemSliding } from '@ionic/angular';
import * as moment from 'moment';
import { DataService } from '../../services/data.service';
import { PaydayInfo } from '../../models/paydays';
import { ModalController, Platform } from '@ionic/angular';
import { EventPage } from '../event/event.page';

@Component({
  selector: 'app-segment',
  templateUrl: './segment.component.html',
  styleUrls: ['./segment.component.scss'],
})
export class SegmentComponent implements OnInit {
  // @ViewChild('slidingItem') slidingItem: IonItemSliding;
  payday: PaydayInfo;
  category: string;
  selfEvent = false;
  constructor(
    public data: DataService,
    private modalCtrl: ModalController,
    private platform: Platform
  ) {}

  ngOnInit() {}
  async removeItem(item: any, event: any) {
    let slide: number;
    await event.getSlidingRatio().then((ratio) => {
      slide = ratio;
      return slide;
    });
    if (slide >= 0.5) {
      for (let i = 0; i < this.data.event.eventSource.length; i++) {
        if (this.data.event.eventSource[i] === item) {
          this.data.deleteEvent(i);
        }
      }
    }
  }
  async modalEvent(clickEvent) {
    const popover = await this.modalCtrl.create({
      component: EventPage,
      componentProps: {
        selectedDay: this.data.date.currentDay,
        aDay: moment(clickEvent).date(),
        popoverController: false,
        event: clickEvent,
      },
      animated: true,
      cssClass: 'my-custom-modalCtrl-css',
    });
    await popover.present();

    popover.onDidDismiss().then((data: any) => {
      if (data.data) {
        const eventData: any = data.data.event;
        eventData.startTime = new Date(data.data.event.startTime);
        eventData.endTime = new Date(data.data.event.endTime);
        this.data.editEvent(data.data.event, data.data.newEvent);
      } else {
        this.data.setEventStorage();
      }
    });
  }
  segmentChanged(event: any) {
    if (event.detail.value === 'self-event') {
      this.selfEvent = true;
    } else {
      this.selfEvent = false;
    }
    this.category = event.detail.value;
  }
}
