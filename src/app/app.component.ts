import { Component } from '@angular/core';

import { StorageService } from './common/services/storage.service';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { relativeTimeThreshold } from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private mobileAccessibility: MobileAccessibility,
    private storage: StorageService
  ) {
    this.storage.welcome();
    this.initializeApp();
  }
  initializeApp() {
    // Use matchMedia to check the user preference
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');

    toggleDarkTheme(prefersDark.matches);
    prefersDark.addEventListener('change', (event) => {
      toggleDarkTheme(event.matches);
    });

    // Add or remove the "dark" class based on if the media query matches
    function toggleDarkTheme(shouldAdd) {
      document.body.classList.toggle('dark', shouldAdd);
    }
    this.platform.ready().then(() => {
      if (this.mobileAccessibility) {
        this.mobileAccessibility.usePreferredTextZoom(false);
      }
    });
    this.statusBar.styleDefault();
    this.splashScreen.hide();
  }
}
