import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../common/pipe/pipes.module';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { CalendarPage } from '../common/pages/calendar/calendar.page';
import { SegmentComponent } from '../common/pages/segment/segment.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    PipesModule
  ],
  declarations: [HomePage, CalendarPage, SegmentComponent]
})
export class HomePageModule {}
