import { Component, ChangeDetectorRef, OnChanges } from '@angular/core';
import { ModalController, PopoverController, Platform } from '@ionic/angular';
import { DataService } from '../common/services/data.service';
import { EventPage } from '../common/pages/event/event.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  oren: any;
  constructor(
    private data: DataService,
    private popoverCtrl: PopoverController,
    private router: Router,
    private modalCtrl: ModalController,
    private platform: Platform,
    private change: ChangeDetectorRef
  ) {
    // this.platform.resize.subscribe(async () => {
    //   if (this.platform.is('ios')) {
    //     const elementHeight = this.platform.height() - 420;
    //     document.documentElement.style.setProperty(
    //       '--segment-height',
    //       `${elementHeight}px`
    //     );
    //   } else if (this.platform.is('android')) {
    //     const elementHeight = this.platform.height() - 399;
    //     document.documentElement.style.setProperty(
    //       '--segment-height',
    //       `${elementHeight}px`
    //     );
    //   } else {
    //     const elementHeight = this.platform.height() - 405;
    //     document.documentElement.style.setProperty(
    //       '--segment-height',
    //       `${elementHeight}px`
    //     );
    //   }
    // });
  }
  ionViewWillEnter() {
    this.platform.resume.subscribe(async () => {
      const t = new Date();
      this.data.currentDay = t;
      // this.data.changeMonth(t);
      this.change.detectChanges();
    });
    if (this.platform.is('ios')) {
      if (this.platform.height() < 800) {
        const elementHeight = this.platform.height() - 420;
        document.documentElement.style.setProperty(
          '--segment-height',
          `${elementHeight}px`
        );
      } else {
        const elementHeight = this.platform.height() - 440;
        document.documentElement.style.setProperty(
          '--segment-height',
          `${elementHeight}px`
        );
      }
    } else if (this.platform.is('android')) {
      const elementHeight = this.platform.height() - 400;
      document.documentElement.style.setProperty(
        '--segment-height',
        `${elementHeight}px`
      );
    } else {
      const elementHeight = this.platform.height() - 420;
      document.documentElement.style.setProperty(
        '--segment-height',
        `${elementHeight}px`
      );
    }
  }
  async addEvent() {
    const popover = await this.modalCtrl.create({
      component: EventPage,
      componentProps: {
        selectedDay: this.data.date.currentDay,
        modalController: true,
      },
      cssClass: 'my-custom-modalCtrl-css',
    });
    await popover.present();
    popover.onDidDismiss().then((data: any) => {
      if (data.data) {
        const eventData: any = data.data.event;
        eventData.startTime = new Date(data.data.event.startTime);
        eventData.endTime = new Date(data.data.event.endTime);
        if (this.data.event.eventSource != null) {
          const events = this.data.event.eventSource;
          events.push(eventData);
          this.data.saveEvent(events);
        } else {
          this.data.saveEvent(eventData);
          console.log(eventData);
        }
      }
    });
  }
  gotoWelcome() {
    this.router.navigateByUrl('/welcome');
  }
}
