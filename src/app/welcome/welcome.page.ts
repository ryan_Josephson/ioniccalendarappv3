import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../common/services/data.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { PopoverController } from '@ionic/angular';
import { PurchasePage } from '../common/pages/purchase/purchase.page';
import { AlertPage } from '../common/pages/alert/alert.page';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { StorageService } from '../common/services/storage.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
  providers: [EmailComposer, InAppPurchase, InAppBrowser],
})
export class WelcomePage implements OnInit {
  selectEvent: string;
  color: any[];
  selectColor: any[] = [];
  // toggle: any = document.querySelector('#themeToggle');
  darkMode: boolean;

  constructor(
    private router: Router,
    private data: DataService,
    private emailComposer: EmailComposer,
    private storage: StorageService,
    private iap: InAppPurchase,
    private popCtrl: PopoverController,
    private iab: InAppBrowser
  ) {}

  ngOnInit() {
    // this.welcome();
    this.selectingColor();
    // this.darkMode = document.body.className === 'dark' ? true : false;
  }
  ionViewWillEnter() {
    this.data.getDistrictColor();
    // this.data.getDarkMode();
  }
  ionViewDidEnter() {
    this.color = this.data.color;
    this.darkMode = this.data.darkMode;
    if (!this.darkMode) {
      document.body.classList.remove('dark');
      // document.body.classList.remove('md');
    } else {
      document.body.classList.add('dark');
      // document.body.classList.add('md');
    }
    this.selectEvent = this.data.selectEvent;
    this.checkTheme();
  }
  async checkTheme() {
    const theme = await this.storage.get('theme');
    if (!theme) {
      const data = {
        color: this.color,
        selectEvent: this.selectEvent,
      };
      this.storage.set('theme', data);
    }
  }
  // welcome() {
  //   const welcome: any = this.wasWelcomed().then(value => {
  //     if(value) {
  //       this.router.navigateByUrl('/home');
  //     } else {
  //       this.setWelcomed();
  //     }
  //   });
  // }
  // async wasWelcomed() {
  //   return await this.storage.get('wasWelcomeOnce');
  // }
  // async setWelcomed() {
  //   await this.storage.set('wasWelcomeOnce', true);
  //   // this.storage.clear();
  // }
  // blue, green, brown, red, black, yellow - branch 4985, 4682, 483, 385, 373, 142, 79
  // black, red, blue, yellow, green, purple- branch 6000, 1913, 529,
  // black, red, blue, brown, green, purple - branch 3126, 828
  // yellow, brown, orange, red, green, blue - branch 214
  toggleDark() {
    if (this.darkMode) {
      document.body.classList.remove('dark');
      // document.body.classList.remove('md');
    } else {
      document.body.classList.add('dark');
      // document.body.classList.add('md');
    }
  }

  selectingColor() {
    this.selectColor.push([
      '#4d4dff',
      'green',
      '#876418',
      'red',
      'black',
      '#FFEC00',
    ]);
    this.selectColor.push([
      'black',
      'red',
      '#4d4dff',
      'gold',
      'green',
      '#8F03FF',
    ]);
    this.selectColor.push([
      'black',
      'red',
      '#4d4dff',
      '#876418',
      'green',
      '#8F03FF',
    ]);
    this.selectColor.push([
      '#FFEC00',
      '#876418',
      'orange',
      'red',
      'green',
      '#4d4dff',
    ]);
  }
  gotoHome() {
    this.data.darkMode = this.darkMode;
    const data = {
      color: this.color,
      selectEvent: this.selectEvent,
    };
    // this.data.setWelcomeStatus();
    this.storage.set('theme', data);
    this.storage.set('darkmode', this.darkMode);
    this.router.navigateByUrl('/home');
  }
  async selectAlert() {
    const popover = await this.popCtrl.create({
      component: AlertPage,
      componentProps: {
        color: this.selectColor,
      },
      cssClass: 'my-custom-select-css',
    });
    await popover.present();
    popover.onDidDismiss().then((data: any) => {
      if (data.data) {
        this.color = data.data;
        this.data.setDistrictColor(this.color);
      }
    });
  }
  async purchase() {
    const popover = await this.popCtrl.create({
      component: PurchasePage,
      componentProps: {},
      cssClass: 'my-custom-purchase-css',
    });
    await popover.present();
  }
  contact() {
    this.emailComposer.isAvailable().then((available: boolean) => {
      if (available) {
        const email = {
          to: 'PostaCalendar@gmail.com',
          subject: 'Postal Calendar',
          body: 'Let me Know what you think!',
          isHtml: true,
        };
        this.emailComposer.open(email);
      }
    });
  }
  policy() {
    const browser = this.iab.create(
      'https://sites.google.com/view/rjspostalcalendarprivacypolicy/home',
      '_system',
      'usewkwebview=yes'
    );

    browser.show();
  }
}
